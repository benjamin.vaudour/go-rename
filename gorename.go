package main

import (
	"fmt"
	"os"
	"strings"

	"framagit.org/benjamin.vaudour/shell/console"
	"framagit.org/benjamin.vaudour/strutil/align"
	"framagit.org/benjamin.vaudour/strutil/style"
	"github.com/rwtodd/Go.Sed/sed"
)

func exists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func main() {
	if len(os.Args) <= 2 {
		fmt.Fprintln(os.Stderr, style.Format("Veuillez saisir au moins 2 arguments", "l_red"))
		return
	}
	engine, err := sed.New(strings.NewReader(os.Args[1]))
	if err != nil {
		fmt.Fprintln(os.Stderr, style.Format(err.Error(), "l_red"))
		return
	}
	var replaces [][]string
	n := 0
	for _, e1 := range os.Args[2:] {
		e1 = strings.TrimSpace(e1)
		if !exists(e1) {
			continue
		}
		if e2, err := engine.RunString(e1); err == nil {
			e2 = strings.TrimSpace(e2)
			if e1 == e2 || len(e2) == 0 {
				continue
			}
			replaces = append(replaces, []string{e1, e2})
			if l := len(e1); l > n {
				n = l
			}
		}
	}
	if len(replaces) == 0 {
		fmt.Fprintln(os.Stderr, style.Format("Aucun fichier à remplacer", "yellow"))
		return
	}
	for _, r := range replaces {
		fmt.Printf("%s → %s\n", align.Left(r[0], n), r[1])
	}
	if console.QuestionBool("Remplacer ?", true) {
		for _, r := range replaces {
			os.Rename(r[0], r[1])
		}
	} else {
		fmt.Fprintln(os.Stderr, style.Format("Opération annulée", "yellow"))
	}
}
