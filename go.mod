module framagit.org/benjamin.vaudour/go-rename

go 1.14

require (
	framagit.org/benjamin.vaudour/shell v1.0.0
	framagit.org/benjamin.vaudour/strutil v1.0.1
	github.com/rwtodd/Go.Sed v0.0.0-20190103233418-906bc69c9394
)
